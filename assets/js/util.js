/**
 * Created by thickam on 8/20/15.
 */
function getClassName(classCode) {
  if (classCode === 1) {
    return "warrior";
  } else if (classCode === 2) {
    return "paladin";
  } else if (classCode === 3) {
    return "hunter";
  } else if (classCode === 4) {
    return "rogue";
  } else if (classCode === 5) {
    return "priest";
  } else if (classCode === 6) {
    return "death-knight";
  } else if (classCode === 7) {
    return "shaman";
  } else if (classCode === 8) {
    return "mage";
  } else if (classCode === 9) {
    return "warlock";
  } else if (classCode === 10) {
    return "monk";
  } else if (classCode === 11) {
    return "druid";
  }
}

function itemNeedsEnchant(slotName) {
  if (slotName == "Neck" || slotName == "Back" || slotName == "Finger 1" || slotName == "Finger 2" || slotName == "Main Hand") {
    return true;
  }
}

function getItemRarity(item) {
  if (item.quality == 0) {
    return "poor";
  } else if (item.quality == 1) {
    return "common";
  } else if (item.quality == 2) {
    return "uncommon";
  } else if (item.quality == 3) {
    return "rare";
  } else if (item.quality == 4) {
    return "epic";
  } else if (item.quality == 5) {
    return "legendary";
  } else if (item.quality == 6) {
    return "artifact"
  } else if (item.quality == 7) {
    return "heirloom"
  } else if (item.quality == 8) {
    return "quality_8"
  } else if (item.quality == 9) {
    return "quality_9"
  }
}

function inventoryTypeToString(number) {
  if (number === 0) {
    return "None";
  } else if (number === 1) {
    return "Head";
  } else if (number === 2) {
    return "Neck";
  } else if (number === 3) {
    return "Shoulder";
  } else if (number === 4) {
    return "Shirt";
  } else if (number === 5) {
    return "Chest";
  } else if (number === 6) {
    return "Waist";
  } else if (number === 7) {
    return "Legs";
  } else if (number === 8) {
    return "Feet";
  } else if (number === 9) {
    return "Wrist";
  } else if (number === 10) {
    return "Hands";
  } else if (number === 11) {
    return "Finger";
  } else if (number === 12) {
    return "Trinket";
  } else if (number === 13) {
    return "One-Hand";
  } else if (number === 14) {
    return "Shield";
  } else if (number === 15) {
    return "Ranged";
  } else if (number === 16) {
    return "Cloak";
  } else if (number === 17) {
    return "Two-Hand";
  } else if (number === 18) {
    return "Bag";
  } else if (number === 19) {
    return "Tabard";
  } else if (number === 20) {
    return "Robe";
  } else if (number === 21) {
    return "Main Hand";
  } else if (number === 22) {
    return "Off Hand";
  } else if (number === 23) {
    return "Held In Off-hand";
  } else if (number === 24) {
    return "Ammo";
  } else if (number === 25) {
    return "Thrown";
  } else if (number === 26) {
    return "Ranged Right";
  } else if (number === 27) {
    console.log('ERROR IN util.inventoryTypeToString(' + number + ')');
    return "ERROR";
  } else if (number === 28) {
    return "Relic";
  } else {
    console.log('ERROR IN util.inventoryTypeToString(27)');
    return "ERROR";
  }

  function itemClassToString(number) {
    if (number.contains('.')) {
      number = number.split('.')[0]
    }
    if (number === 0) {
      return "Consumable";
    } else if (number === 1) {
      return "Container";
    } else if (number === 2) {
      return "Weapon";
    } else if (number === 3) {
      return "Gem";
    } else if (number === 4) {
      return "Armor";
    } else if (number === 5) {
      return "Reagent";
    } else if (number === 6) {
      return "Projectile";
    } else if (number === 7) {
      return "Trade Goods";
    } else if (number === 8) {
      return "Generic";
    } else if (number === 9) {
      return "Book";
    } else if (number === 10) {
      return "Money";
    } else if (number === 11) {
      return "Quiver";
    } else if (number === 12) {
      return "Quest";
    } else if (number === 13) {
      return "Key";
    } else if (number === 14) {
      return "Permanent";
    } else if (number === 15) {
      return "Junk";
    } else if (number === 16) {
      return "Glyph";
    } else {
      console.log('ERROR IN util.inventoryTypeToString(' + number + ')');
      return "ERROR";
    }
  }

  /**
   *
   * @param number {float} Needs to be full decimal identifier
   * @returns {*}
   */
  function itemSubclassToString(number) {
    if (itemClass.contains('.')) {
      var itemClass = itemClass.split('.')[0];
      itemSubClass = itemClass.split('.')[1];
    }
    if (itemClass === 0) {
      if (itemSubClass === 1) {
        return "Potion";
      } else if (itemSubClass === 2) {
        return "Elixir";
      } else if (itemSubClass === 3) {
        return "Flask";
      } else if (itemSubClass === 4) {
        return "Scroll";
      } else if (itemSubClass === 5) {
        return "Food/Water";
      } else if (itemSubClass === 6) {
        return "Enchantment";
      } else if (itemSubClass === 7) {
        return "Bandage";
      } else {
        console.log('ERROR IN util.itemSubclassToString(' + number + ')');
        return "ERROR";
      }
    }
    else if (itemClass === 1) {
      if (itemSubClass === 0) {
        return "Bag"
      } else if (itemSubClass === 2) {
        return "Herbalism";
      } else if (itemSubClass === 3) {
        return "Enchanting";
      } else if (itemSubClass === 4) {
        return "Engineering";
      } else if (itemSubClass === 5) {
        return "Jewelcrafting";
      } else if (itemSubClass === 6) {
        return "Mining";
      } else if (itemSubClass === 7) {
        return "Leatherworking";
      } else if (itemSubClass === 8) {
        return "Inscription";
      } else if (itemSubClass === 9) {
        return "Fishing";
      } else {
        console.log('ERROR IN util.itemSubclassToString(' + number + ')');
        return "ERROR";
      }
    }
    else if (itemClass === 2) {
      if (itemSubClass === 1) {
        return "1H Axe";
      } else if (itemSubClass === 2) {
        return "2H Axe";
      } else if (itemSubClass === 3) {
        return "Bow";
      } else if (itemSubClass === 4) {
        return "Rifle";
      } else if (itemSubClass === 5) {
        return "1H Mace";
      } else if (itemSubClass === 6) {
        return "2H Mace";
      } else if (itemSubClass === 7) {
        return "Polearm";
      } else if (itemSubClass === 8) {
        return "1H Sword";
      } else if (itemSubClass === 9) {
        return "2H Sword";
      } else if (itemSubClass === 10) {
        return "Staff";
      } else if (itemSubClass === 11) {
        return "1H Exotic";
      } else if (itemSubClass === 12) {
        return "2H Exotic";
      } else if (itemSubClass === 13) {
        return "Fist";
      } else if (itemSubClass === 14) {
        return "Miscellaneous";
      } else if (itemSubClass === 15) {
        return "Dagger";
      } else if (itemSubClass === 16) {
        return "Thrown";
      } else if (itemSubClass === 17) {
        return "Spear";
      } else if (itemSubClass === 18) {
        return "Crossbow";
      } else if (itemSubClass === 19) {
        return "Wand";
      } else if (itemSubClass === 20) {
        return "Fishing Pole";
      } else {
        console.log('ERROR IN util.itemSubclassToString(' + number + ')');
        return "ERROR";
      }
    }
    else if (itemClass === 4) {
      if (itemSubClass === 0) {
        return "Miscellaneous";
      } else if (itemSubClass === 1) {
        return "Cloth";
      } else if (itemSubClass === 2) {
        return "Leather";
      } else if (itemSubClass === 3) {
        return "Mail";
      } else if (itemSubClass === 4) {
        return "Plate";
      } else if (itemSubClass === 6) {
        return "Shield";
      }
    }
    else if (itemClass === 7) {
      if (itemSubClass === 1) {
        return "Parts"
      } else if (itemSubClass === 2) {
        return "Explosives";
      } else if (itemSubClass === 3) {
        return "Devices";
      } else if (itemSubClass === 4) {
        return "Jewelcrafting";
      } else if (itemSubClass === 5) {
        return "Tailoring";
      } else if (itemSubClass === 6) {
        return "Leatherworking";
      } else if (itemSubClass === 7) {
        return "Mining";
      } else if (itemSubClass === 8) {
        return "Cooking";
      } else if (itemSubClass === 9) {
        return "Herbing";
      } else if (itemSubClass === 10) {
        return "Elements";
      } else if (itemSubClass === 12) {
        return "Enchanting";
      } else {
        console.log('ERROR IN util.itemSubclassToString(' + number + ')');
        return "ERROR";
      }
    }
    else if (itemClass === 9) {
      if (itemSubClass === 1) {
        return "Leatherworking"
      } else if (itemSubClass === 2) {
        return "Tailoring";
      } else if (itemSubClass === 3) {
        return "Engineering";
      } else if (itemSubClass === 4) {
        return "Blacksmithing";
      } else if (itemSubClass === 5) {
        return "Cooking";
      } else if (itemSubClass === 6) {
        return "Alchemy";
      } else if (itemSubClass === 7) {
        return "First Aid";
      } else if (itemSubClass === 8) {
        return "Enchanting";
      } else if (itemSubClass === 9) {
        return "Fishing";
      } else if (itemSubClass === 10) {
        return "Jewelcrafting";
      } else if (itemSubClass === 11) {
        return "Inscription";
      } else {
        console.log('ERROR IN util.itemSubclassToString(' + number + ')');
        return "ERROR";
      }
    }
    else if (itemClass === 15) {
      if (itemSubClass === 2) {
        return "Pets";
      } else if (itemSubclass === 3) {
        return "Holiday";
      } else if (itemSubclass === 5) {
        return "Mount";
      }
    }
    else if (itemClass === 16) {
      if (itemSubClass === 1) {
        return "Warrior"
      } else if (itemSubClass === 2) {
        return "Paladin";
      } else if (itemSubClass === 3) {
        return "Hunter";
      } else if (itemSubClass === 4) {
        return "Rogue";
      } else if (itemSubClass === 5) {
        return "Priest";
      } else if (itemSubClass === 6) {
        return "Death Knight";
      } else if (itemSubClass === 7) {
        return "Shaman";
      } else if (itemSubClass === 8) {
        return "Mage";
      } else if (itemSubClass === 9) {
        return "Warlock";
      } else if (itemSubClass === 10) {
        return "Monk";
      } else if (itemSubClass === 11) {
        return "Druid";
      } else {
        console.log('ERROR IN util.itemSubclassToString(' + number + ')');
        return "ERROR";
      }
    }
    else {
      console.log('ERROR IN util.inventoryTypeToString(' + itemClass + ')');
      return "ERROR";
    }
  }
}
