/**
 * Created by thickam on 8/23/15.
 */
app.controller('BossPrioritizerController', function ($scope) {

  $scope.raids = constants.raids;

  $scope.selectedRaid = "Select Raid:";

  $scope.selectRaid = function selectedRaid(raid) {
    $scope.selectedRaid = raid;
  };

  $scope.difficulties = {
    "normal": {
      name: "normal",
      value: false
    },
    "heroic": {
      name: "heroic",
      value: false
    },
    "mythic": {
      name: "mythic",
      value: false
    }
  };

  $scope.toggleDifficulty = function(difficulty) {
    if ($scope.difficulties[difficulty].value) {
      $scope.difficulties[difficulty].value = false;
    } else {
      $scope.difficulties[difficulty].value = true;
    }
  };

  $scope.upgradeDisplayType = "most";

  $scope.displayType = {
    most: "most",
    mostPerLoot: "mostPerLoot",
    mostQuality: "mostQuality",
    all: "all"
  };

  $scope.findUpgrades = function findUpgrades(){
    $scope.selectedRaid = "Select Raid:"
  };
});
