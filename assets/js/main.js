/**
 * Created by thickam on 8/20/15.
 */


app.run(function($rootScope) {
});

app.controller('MyController', function ($scope) {
  $scope.error = false;
  $scope.characterLoading = false;
  //$scope.characterLoaded = false;
  //FOR DEBUGGING AND STYLING ONLY, REMOVE LATER
  $scope.name = "Brejwelb";
  $scope.realm = "Bladefist";
  $scope.locale = "en-US";
  $scope.characterLoaded = true;
  //
  //$scope.character = {
  //  thumbnail: "kul-tiras/150/107432086-avatar.jpg"
  //};
  //$scope.className = "druid";
  //$scope.selectedTalent = {
  //  spec: {
  //    icon: "spell_nature_starfall"
  //  }
  //};
  //END DEBUGGING AND STYLING MODS
  $scope.getItemQuality = function(item) {
    if (!item.hasOwnProperty("quality")) {
      return null;
    }
    var rarity = getItemRarity(item);
    return rarity;
  };
  $scope.tableAudit = function tableAudit(item, index) {
    if (typeof item !== "number") {
      if (itemNeedsEnchant($scope.getSlotName(index))) {
        if (!item.tooltipParams.hasOwnProperty('enchant')) {
          return "enchant-needed";
        }
      }
    }

  };
  $scope.bonusIdFormatter = function(item) { //TODO: add checking to see if array exists
    if (!item.hasOwnProperty("bonusLists")) {
      return;
    }
    if (!item.bonusLists.length){
      return;
    }
    var concatenatedList = "";
    item.bonusLists.forEach(function(currentValue, index, array) {
      concatenatedList += currentValue + ':';
    });
    return concatenatedList.slice(0,-1); //trims the last character (a semicolon that's unneeded)
  };
  $scope.getSlotName = function(index) {
    var propNameArray = [];
    for (var prop in $scope.character.items){
      propNameArray.push(prop);
    }
    return propNameArray[index]//thanks Vencent Robert on Stack Overflow: http://stackoverflow.com/questions/4149276/javascript-camelcase-to-regular-form
      .replace(/([A-Z0-9])/g, ' $1')
      .replace(/^./, function(str){ return str.toUpperCase(); });
  };
  $scope.submit = function() {
    $scope.characterLoading = true;
    $.ajax({
      url: "https:/us.api.battle.net/wow/character/" + $scope.realm + "/" + $scope.name + "?fields=stats,talents,items&locale=" + $scope.locale + "&apikey=" + "gcghfeme2e3f6z693e5q9uum5s9zdyna",
      error: function(){ //This needs to be more detailed
        $scope.$apply(function() {
        $scope.error = true;
        $scope.characterLoading = false;
        });
      },
      success: function(data){
        $scope.$apply(function() {
          $scope.error = false;
          $scope.characterLoading = false;
          $scope.characterLoaded = true;
          $scope.character = data;
          $scope.selectedTalent = (data.talents[0].selected !== undefined) ? (data.talents[0]) : (data.talents[1]);
          $scope.className = getClassName(data.class);
        });
      }
    });
  };
});
