# boss-prioritizer

So, the end goal is to have two functionalities:

1. Look up one character and rank bosses in different raids accordingly

2. Do the same for multiple characters

  I would like both to have a decent looking UI but that will come closer to the end once we get models and utility functions set up.

  looking to the future I'd like:
    * Ability to choose raid/difficulty
    * Checkboxes to choose who in the list should be considered
    * Select by character to find just their upgrades
    * edit stat weights

The first step is going to be setting up models. They should mock all of the relevant data ([Battle.Net API](https://dev.battle.net/io-docs)). This will include:

[Here](http://freetexthost.com/h0mmwtvzr1) is a link to a sample string we are pulling down. This can be taken to a
json formatter (I use [this one](https://jsonformatter.curiousconcept.com/)) to make it collapse-able and easier to view.

  * character
    * name
    * realm
    * class
    * race
    * calcClass
    * items
      * averageItemLevel
      * averageItemLevelEquipped
      * Head
        * id
        * name
        * icon
        * quality
        * itemLevel
        * tooltipParams
          * set //(array of int)
          * transmogItem
          * timewalkerLevel
        * stats //(array of objects)
          * stat
          * amount
        * armor
        * context
        * bonusLists //(array of int)
    * stats
      * health
      * powerType
      * power
      * str
      * agi
      * int
      * sta
      * crit
      * critRating
      * haste
      * hasteRating
      * hasteRatingPercent
      * mastery
      * masteryRating
      * spr
      * bonusArmor
      * multistrike
      * multistrikeRating
      * multistrikeRatingBonus
      * leech
      * leechRating
      * leechRatingBonus
      * versatility
      * versatilityDamageDoneBonus
      * versatilityHealingDoneBonus
      * versatilityDamageTakenBonus
      * avoidanceRating
      * avoidanceRatingBonus
      * spellPower
      * spellPen
      * spellCrit
      * spellCritRating
      * mana5
      * mana5Combat
      * armor
      * dodge
      * dodgeRating
      * parry
      * parryRating
      * block
      * blockRating
      * mainHandDmgMin
      * mainHandDmgMax
      * mainHandSpeed
      * mainHandDps
      * offHandDmgMin
      * offHandDmgMax
      * offHandSpeed
      * offHandDps
      * rangedDmgMin
      * rangedDmgMax
      * rangedSpeed
      * rangedDps
      * attackPower
      * rangedAttackPower
    * talents (array of two)
      * selected
      * talents
        * asdf
      * glyphs
        * asdf
      * spec
        * name
        * role
        * backgroundImage
        * icon
        * description
        * order
      * calcTalent
      * calcSpec
      * calcGlyph


  * item //(item query for raid items' syntax is itemId/raidDifficulty raidDifficulties are "raid-normal", "raid-heroic", and "raid-mythic")
    * id
    * name
    * icon
    * bonusStats
      * stat
      * amount
    * itemClass
    * itemSubclass
    * weaponInfo
      * damage
        * min
        * max
        * exactMin
        * exactMax
      * weaponSpeed
      * dps
    * inventoryType
    * equippable
    * itemLevel
    * baseArmor
    * hasSockets
    * armor


There will need to be disambiguation between items that come from the character profile and items queried with an id.

a [Sails](http://sailsjs.org) application
